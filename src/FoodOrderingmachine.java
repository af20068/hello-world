import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingmachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton edamameButton;
    private JButton gyozaButton;
    private JButton yakitoriButton;
    private JButton karaageButton;
    private JButton beerButton;

    private JLabel Total;

    private JButton checkOutButton;
    private JLabel kingaku;
    private JLabel tyumonsu;
    private JTextPane nedan;
    private JTextPane kazu;
    private JTextPane recievedinfo;


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingmachine");
        frame.setContentPane(new FoodOrderingmachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    int sumprice = 0;
    int[] a = new int[6]; //number of orders
    int b = 0;

    void order(String food, int price) {
        JOptionPane pane = new JOptionPane();
        int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            if (food == "Beer") {
                int confirmation2 = JOptionPane.showConfirmDialog(null, "Are you over 20 years old?",
                        "Caution!",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation2 == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
                    a[0] += 1;
                    b += 1;
                    String Text = recievedinfo.getText();
                    recievedinfo.setText(Text + food + "\n");
                    String Text2 = nedan.getText();
                    nedan.setText(Text2 + price + "\n");
                    String Text3 = kazu.getText();
                    kazu.setText(Text3 + "1\n");
                    sumprice += price;
                    kingaku.setText(sumprice + " yen");
                    tyumonsu.setText(b + " tsu");
                }
                else if (confirmation2 == 1) {
                    JOptionPane.showMessageDialog(null, "Drinking is after becoming an adult.");
                }
            }
            else {
                switch (food) {
                    case "Tempura":
                        a[1] += 1;
                        break;
                    case "Gyoza":
                        a[2] += 1;
                        break;
                    case "Edamame":
                        a[3] += 1;
                        break;
                    case "Yakitori":
                        a[4] += 1;
                        break;
                    case "Karaage":
                        a[5] += 1;
                        break;
                }
                JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
                b += 1;
                String Text = recievedinfo.getText();
                recievedinfo.setText(Text + food + "\n");
                String Text2 = nedan.getText();
                nedan.setText(Text2 + price + "\n");
                String Text3 = kazu.getText();
                kazu.setText(Text3 + "1\n");
                sumprice += price;
                kingaku.setText(sumprice + " yen");
                tyumonsu.setText(b + " tsu");
            }

        }
    }

    void reset(int x) {
        if (x == 0) {
            sumprice = 0;
            b = 0;
            for (int i = 0; i < 6; i++) {
                a[i] = 0;
            }
            kingaku.setText("0" + " yen");
            tyumonsu.setText("0" + " tsu");
            recievedinfo.setText("");
            nedan.setText("");
            kazu.setText("");
        }
    }

    public FoodOrderingmachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 600);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza", 400);
            }
        });
        edamameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Edamame", 200);
            }
        });
        yakitoriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakitori", 300);
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", 500);
            }
        });
        beerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Beer", 650);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to checkout?\nOrder details:\nTempura " + a[1] + "tsu\nGyoza " + a[2] +
                                "tsu\nEdamame " + a[3] + "tsu\nYakitori " + a[4] + "tsu\nKaraage " + a[5] + "tsu\nBeer " + a[0] + "tsu\n",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0 && sumprice == 0) {
                    JOptionPane.showMessageDialog(null, "Your order has not been completed yet.\n");
                }
                if (confirmation == 0 && sumprice != 0) {
                    JOptionPane.showMessageDialog(null, "Thank you for ordering.\nThe total is " + sumprice + " yen.\n");
                    reset(confirmation);
                }
                if (confirmation == 1) {
                    int confirmation2 = JOptionPane.showConfirmDialog(null, "Would you like to start over？\nIf you select yes, it will be from the beginning.\n",
                            "Caution!",
                            JOptionPane.YES_NO_OPTION);
                    if (confirmation2 == 0) {
                        reset(confirmation2);
                    }
                }
            }
        });
    }
}
